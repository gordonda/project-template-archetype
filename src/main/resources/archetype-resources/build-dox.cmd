call mvn clean
mkdir target
call mvn package -Ddoxygen -DskipTests
pushd target\doxygen\latex
call make
popd
copy /y target\doxygen\latex\refman.pdf .
