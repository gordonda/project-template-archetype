#!/bin/bash

mvn clean
mkdir target
mvn package -Ddoxygen -DskipTests
pushd target/doxygen/latex
make
popd
cp target/doxygen/latex/refman.pdf .
