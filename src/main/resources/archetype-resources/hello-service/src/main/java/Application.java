#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Hello service Spring Boot application.
 */
@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackageClasses = { HelloController.class })
public class Application {

  /**
   * Value for LogContextKeys.APPLICATION_ID.
   */
  public static final String APPLICATION_ID = "${artifactId}";

  /**
   * Hello service Spring Boot application entry point.
   *
   * @param args
   *          Command-line arguments.
   */
  public static void main(String[] args) {

    SpringApplication.run(Application.class, args);

  }

}
