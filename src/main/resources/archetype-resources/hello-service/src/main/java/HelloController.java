#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.io.InputStream;
import org.apache.logging.log4j.CloseableThreadContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * Simple "hello world" service to test build configuration.
 */
@RestController
@EnableAutoConfiguration
@Api(basePath = "/hello", description = "Hello, world!")
public class HelloController {

  private static final Logger LOGGER;

  /**
   * Initialize static fields.
   */
  static {

    LOGGER = LogManager.getLogger(HelloController.class);

  }

  /**
   * Place-holder entry point to test build configuration.
   *
   * @return "Hello, world!"
   */
  @GetMapping("/hello")
  @ApiOperation(value = "Default entry point",
          notes = "Place-holder entry point to test build configuration.")
  public String index() {

    try {

      LOGGER.info("Saying hi");
      return "Hello, world!";

    } catch (Exception e) {

      // note that we deliberately do not allow recoverable errors to propagate
      // as status code 500 ... if the controller is responding, always return
      // status code 200 and indicate the outcome in the response payload!
      LOGGER.error("something bad happened", e);
      return ">>> ERROR <<<";

    }
  }

  /**
   * Upload a file.
   *
   * @param file
   *          The request parameter containing the uploaded file's contents and
   *          metadata
   *
   * @return A diagnostic message
   */
  @PostMapping("/upload")
  @ApiOperation(value = "Upload a file",
          notes = "POST handler for multipart/form-data containing the contents of a file")
  public String upload(@RequestParam("file") MultipartFile file) {

    try {

      final long fileSize = file.getSize();
      final long total = readFile(file);

      if (total != fileSize) {

        throw new Exception(
                "expected " + fileSize + " bytes, got " + total);

      }

      return file.getOriginalFilename() + " (" + file.getContentType() + ")";

    } catch (Exception e) {

      LOGGER.error("error uploading file", e);
      return ">>> ERROR <<<";

    }
  }

  /**
   * Read the contents of the uploaded file using the supplied input stream.
   *
   * @param file
   *          The file stream
   *
   * @return The number of bytes read from the file.
   */
  public long readFile(MultipartFile file) throws IOException {

    final byte[] buffer = new byte[1024];
    long total = 0;

    try (final InputStream stream = file.getInputStream()) {

      int actual;

      while ((actual = stream.read(buffer, 0, buffer.length)) > 0) {

        total += actual;

      }
    }

    return total;
  }

}
