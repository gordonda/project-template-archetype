#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

/*!
${symbol_escape}package ${package}

${symbol_escape}brief Hello Service

Placeholder project created by project archetype. A web service that literally
returns "Hello, world!"
 */
