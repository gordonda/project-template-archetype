#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Unit tests for HelloController.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { HelloController.class })
@AutoConfigureMockMvc
public final class HelloControllerTest {

  @Autowired
  private HelloController helloController;

  /**
   * Demonstrate unit testing for a Spring controller.
   *
   * This shows the use of annotations to invoke the mock Spring container, but
   * otherwise these are just JUnit test procedures.
   */
  @Test
  public void someTest() {

    assertEquals(helloController.index(), "Hello, world!");

  }
}
