#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.net.URL;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Integration tests for HelloController.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = { HelloController.class })
@AutoConfigureMockMvc
public final class HelloControllerIT {

  /**
   * TCP/IP port on which server instance is run.
   */
  @LocalServerPort
  private int              port;

  /**
   * Base URL for servlet instance.
   */
  private URL              base;

  /**
   * REST template for integration tests.
   */
  @Autowired
  private TestRestTemplate template;

  /**
   * Set `base` before invoking test method.
   */
  @Before
  public void before() throws Exception {

    base = new URL("http://localhost:" + port + "/hello");

  }

  /**
   * Demonstrate unit testing for a Spring controller.
   *
   * This shows that integration tests can look exactly like unit tests, except
   * assume the existence of some well-defined environment, e.g. as controlled
   * using Maven's failsafe plugin and its support for pre-integration-test and
   * post-integration test phases.
   */
  @Test
  public void indexIT() {

    final ResponseEntity<String> response = template
            .getForEntity(base.toString(), String.class);
    assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    assertThat(response.getBody(), equalTo("Hello, world!"));

  }
}
