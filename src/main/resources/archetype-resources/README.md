# ${groupId}:${artifactId}:${version}

[Maven](http://maven.apache.org/) based multi-module [Spring Boot](https://projects.spring.io/spring-boot/) project with one child module. That child module implements a literal "Hello, world!" web service.

## Dependencies

### Build-Time

Requires:

| Dependency | Configuration                                                   |
| ---------- | --------------------------------------------------------------- |
| Git        | Add to `PATH`                                                   |
| JDK 8      | Add to `PATH`                                                   |
| Maven      | Add to `PATH`                                                   |
| texlive    | Add to `PATH`                                                   |
| PlantUML   | Set `PLANTUML_JAR` to absolute path of _plantuml.jar_           |
| Doxygen    | Set `DOXYGEN_EXE` to absolute path of _doxygen_ executable file (_doxygen.exe_ on Windows) |
| Docker     | Set `DOCKER_EXE` to absolute path of _docker_ executable file (_docker.exe_ on Windows) |

### Run-Time

- JRE 8
- Docker

## Building

The process running Maven commands, below, must be logged into the Docker repository _hub.docker.com_.

When invoking Maven manually from the command line, simply invoke the following once in the command shell before executing any of the following `mvn ...` commands:

    docker login

To create Docker images and corresponding containers locally:

    mvn clean install -Ddoxygen -Dintegration-test
    docker create -p 8080:8080 --name hello-service hello-service:1.0-SNAPSHOT

Note that you may have to adjust the values of the `-p` parameters in the preceding `docker create ...` commands depending on which TCP/IP ports are free in your local environment.

You can build and test individual containers by cd'ing to a given module directory to build only the corresponding service.

    pushd hello-service
    mvn clean install
    popd
    docker create -p 8080:8080 --name hello-service hello-service:1.0-SNAPSHOT

## Running

    docker start hello-service

Then, open <http://localhost:8080/> in a web browser.

You can view the Swagger documentation / UI at <http://localhost:8080/swagger-ui.html>.

(You will need to adjust the preceding URL's based on the actual port mapping you specified when creating your local containers.)

When finished:

    docker stop hello-service
    docker rm hello-service
    docker rmi hello-service:1.0-SNAPSHOT

If all you want is to test locally by manually invoking Spring Boot executable JAR's:

    mvn clean package
    java -jar hello-service/target/hello-service-1.0-SNAPSHOT.jar

## Testing

The generated POM files implicitly invoke [JUnit](http://junit.org) based unit tests using Maven's _surefire_ plugin. In addition, they include a profile to invoke integration tests using Maven's _failsafe_ plugin when activated by the `integration-test` Java property.

In other words, a command line like

    mvn install

implicitly invokes unit tests. The build will break if any unit tests fail.

On the other hand,

    mvn install -Dintegration-test

invokes both unit and integration tests. The build breaks if any unit or integration tests fail, including due to attempting to run integration tests outside of a suitably configured environment.

## Deploying

To publish the Docker images:

    mvn deploy

You can then delete the images from your local Docker repository and still invoke `docker create ...` as shown above. It will fetch the images from the cloud when creating the local containers.

## Using the Generated Documentation

The `-Ddoxygen` option to the `mvn ...` build invocations shown above will use Doxygen to generate highly enriched, Javadoc-style documentation in both HTML and LaTeX formats. The LaTeX output will include a script for publishing to PDF.

The HTML documentation is rooted at _target/doxygen/html/index.html_.

To create a PDF file:

    mvn clean package -Ddoxygen -DskipTests
    pushd target/doxygen/latex
    make
    popd

The PDF output will be in _target/doxygen/latex/refman.pdf_.
