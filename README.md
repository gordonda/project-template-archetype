# us.rader:project-template-archetype:1.0-SNAPSHOT

Maven archetype for generating the root project for a collection of containerized services.

## Usage

### 1. Configure Local Build environment

| Dependency | Configuration                                                   |
| ---------- | --------------------------------------------------------------- |
| Git        | Add to `PATH`                                                   |
| JDK 8      | Add to `PATH`                                                   |
| Maven      | Add to `PATH`                                                   |
| texlive    | Add to `PATH`                                                   |
| PlantUML   | Set `PLANTUML_JAR` to absolute path of _plantuml.jar_           |
| Doxygen    | Set `DOXYGEN_EXE` to absolute path of _doxygen_ executable file (_doxygen.exe_ on Windows) |
| Docker     | Set `DOCKER_EXE` to absolute path of _docker_ executable file (_docker.exe_ on Windows) |

### 2. Clone Archetype Source Repository

    git clone https://gitlab.com/parasaurolophus/project-template-archetype.git

### 3. Install Archetype in Local Catalog

    pushd project-template-archetype
    mvn install
    popd

### 4. Invoke Archetype

To run the archetype interactively:

    mvn archetype:generate \
      -DarchetypeGroupId=us.rader \
      -DarchetypeArtifactId=project-template-archetype

You will then be prompted to enter group id, artifact id, version etc. for your new project.

Alternatively, you can pass all the required information on the command line. For example, to create a project with Maven coordinates _us.rader.demo:example:1.0-SNAPSHOT_:

    mvn archetype:generate \
      -DarchetypeGroupId=us.rader \
      -DarchetypeArtifactId=project-template-archetype \
      -DgroupId=us.rader.demo \
      -DartifactId=example \
      -Dversion=1.0-SNAPSHOT \
      -DpackageName=us.rader.demo \
      -DinteractiveMode=false

In either case, the result is a multi-module project with the group id, artifact id etc. that you specify. There will be one child module, _hello-service_. It gives all its child modules access to the Spring framework. The _hello-service_ module is a Spring Boot application with a (literal) "Hello, world!" controller end-point. The _pom.xml_ for _hello-service_ uses the provided _Dockerfile_ to build and deploy a Docker image for the Spring Boot application.

See <https://gitlab.com/parasaurolophus/hello-service-archetype> for a companion archetype for creating additional containerized Spring Boot modules in an existing project created by _project-template-archetype_.
